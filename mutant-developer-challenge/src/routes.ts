import { Router } from 'express';

import ApiController from '@controllers/ApiController';
import IntroController from '@controllers/IntroController';

const routes = Router();

routes.get('/', IntroController.pageHome);

routes.get('/websites', ApiController.indexWb);

routes.get('/ordem', ApiController.indexOrder);

routes.get('/suite', ApiController.indexSuite);

export default routes;
