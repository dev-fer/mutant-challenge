import { Request, Response } from 'express';

import api from '../../services/api';

class ApiController {

  async indexWb (req: Request, res: Response) {
    try {
      const response = await api.get('/users');

      return res.json(response.data.map(website => website.website));

    } catch (err) {
      return res.status(400).json({ ops: 'Fetch API not working at all.' });
    }
  }

  async indexOrder (req: Request, res: Response) {
    try {
      const response = await api.get('/users');
      const newPeople = [];

      for (let index = 0; index < response.data.length; index++) {
        var atributos = {
          name: response.data[index].name,
          email: response.data[index].email,
          empresa: response.data[index].company.name,
        }
        newPeople.push(atributos);
      }

      newPeople.sort(function(nomoAnterior, proximoNome){
        var nA = nomoAnterior.name.toLowerCase();
        var pN = proximoNome.name.toLowerCase();
        return (nA < pN) ? -1 : (nA > pN) ? 1 : 0;
      });

      return res.json(newPeople);
    } catch (err) {
      return res.status(400).json({ ops: 'Fetch API not working at all.' });
    }
  }

  async indexSuite (req: Request, res: Response) {
    try {
      const response = await api.get('/users');
      const peopleAdress = [];
      const word = "suite";

      for (let index = 0; index < response.data.length; index++) {
        if (response.data[index].address.suite.toLowerCase().indexOf(word) >= 0) {
          peopleAdress.push(response.data[index]);
        }
      }

      return res.json(peopleAdress);
    } catch (err) {
      return res.status(400).json({ ops: 'Fetch API not working at all.' });
    }
  }

}

export default new ApiController();

