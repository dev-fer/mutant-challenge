import { Request, Response } from 'express';

class IntroController {

  pageHome (req: Request, res: Response) {
    res.render('index')
  }

}

export default new IntroController();

