const request = require('supertest');
import api from '../services/api';
import app from '../app';

describe('Connection tests and routes return using API', () => {

  it('async API call with Jest', async () => {
    const response = await api.get('/users');
    const underTest = response.data[0];

    expect(underTest.name).toBe('Leanne Graham');
  });

  it('Testing websites route', function(done) {
    request(app)
      .get('/websites')
      .set('Accept', 'application/json')
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        done()
      });
  });

  it('Testing order route', function(done) {
    request(app)
      .get('/ordem')
      .set('Accept', 'application/json')
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        done()
      });
  });

  it('Testing suite route', function(done) {
    request(app)
      .get('/suite')
      .set('Accept', 'application/json')
      .expect(200)
      .end(function(err, res){
        if (err) return done(err);
        done()
      });
  });

});



