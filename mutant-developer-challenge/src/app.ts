var apm = require('elastic-apm-node').start({
  serviceName: 'mutant-app',

  secretToken: '',

  serverUrl: 'http://apm-server:8200/'
});

import express from 'express';
import ejs from 'ejs';
import path from 'path';
import routes from './routes';

class App {
  server: any;
  constructor() {
    this.server = express();

    this.middlewares();
    this.routes();
  }

  middlewares() {
    this.server.use(express.json());
    this.server.set('view engine', 'ejs');
    this.server.set('views', path.join(__dirname,'views'));
    this.server.use(express.static(path.join(__dirname,"public")));
  }

  routes() {
    this.server.use(routes)
  }
}

export default new App().server;
