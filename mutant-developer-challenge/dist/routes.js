"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = require("express");

var _ApiController = _interopRequireDefault(require("./app/controllers/ApiController"));

var _IntroController = _interopRequireDefault(require("./app/controllers/IntroController"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const routes = (0, _express.Router)();
routes.get('/', _IntroController.default.pageHome);
routes.get('/websites', _ApiController.default.indexWb);
routes.get('/ordem', _ApiController.default.indexOrder);
routes.get('/suite', _ApiController.default.indexSuite);
var _default = routes;
exports.default = _default;