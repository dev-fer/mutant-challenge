"use strict";

var apm = require('elastic-apm-node').start({
  serviceName: 'mutant-app',
  secretToken: '',
  serverUrl: 'http://apm-server:8200/'
});

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = _interopRequireDefault(require("express"));

var _path = _interopRequireDefault(require("path"));

var _routes = _interopRequireDefault(require("./routes"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class App {
  constructor() {
    this.server = (0, _express.default)();
    this.middlewares();
    this.routes();
  }

  middlewares() {
    this.server.use(_express.default.json());
    this.server.set('view engine', 'ejs');
    this.server.set('views', _path.default.join(__dirname, 'views'));
    this.server.use(_express.default.static(_path.default.join(__dirname, "public")));
  }

  routes() {
    this.server.use(_routes.default);
  }

}

var _default = new App().server;
exports.default = _default;
