"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

class IntroController {
  pageHome(req, res) {
    res.render('index');
  }

}

var _default = new IntroController();

exports.default = _default;