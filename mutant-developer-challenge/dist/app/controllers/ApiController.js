"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _api = _interopRequireDefault(require("../../services/api"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class ApiController {
  async indexWb(req, res) {
    try {
      const response = await _api.default.get('/users');
      return res.json(response.data.map(website => website.website));
    } catch (err) {
      return res.status(400).json({
        ops: 'Fetch API not working at all.'
      });
    }
  }

  async indexOrder(req, res) {
    try {
      const response = await _api.default.get('/users');
      const newPeople = [];

      for (let index = 0; index < response.data.length; index++) {
        var atributos = {
          name: response.data[index].name,
          email: response.data[index].email,
          empresa: response.data[index].company.name
        };
        newPeople.push(atributos);
      }

      newPeople.sort(function (nomoAnterior, proximoNome) {
        var nA = nomoAnterior.name.toLowerCase();
        var pN = proximoNome.name.toLowerCase();
        return nA < pN ? -1 : nA > pN ? 1 : 0;
      });
      return res.json(newPeople);
    } catch (err) {
      return res.status(400).json({
        ops: 'Fetch API not working at all.'
      });
    }
  }

  async indexSuite(req, res) {
    try {
      const response = await _api.default.get('/users');
      const peopleAdress = [];
      const word = "suite";

      for (let index = 0; index < response.data.length; index++) {
        if (response.data[index].address.suite.toLowerCase().indexOf(word) >= 0) {
          peopleAdress.push(response.data[index]);
        }
      }

      return res.json(peopleAdress);
    } catch (err) {
      return res.status(400).json({
        ops: 'Fetch API not working at all.'
      });
    }
  }

}

var _default = new ApiController();

exports.default = _default;