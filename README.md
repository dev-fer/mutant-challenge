# Getting Started with mutant-developer-challenge

![Logotipo da Mutant](/github/_logotipo.png)

Participação do processo seletivo para ser um desenvolvedor da Mutant.

## 📝 Começando

Essas instruções fornecerão uma cópia do projeto em execução na sua máquina local para fins de avaliação.

## 💻 Tecnologias

Esse projeto foi desenvolvido com todas essas tecnologias:

- [Vagrant](https://www.vagrantup.com/)
- [Docker](https://www.docker.com/)
- [Express](https://expressjs.com/pt-br/)
- [Yarn](https://yarnpkg.com/)
- [Elasticsearch](https://www.elastic.co/pt/elasticsearch/)
- [Jest](https://jestjs.io/pt-BR/)

## 📖 Sobre a tarefa

A tarefa consiste em fazer um aplicativo que carregue a saida da URL
https://jsonplaceholder.typicodecom/users, que retorna uma lista de usuário em JSON.

A utilização do **Vagrant** para uma virtualização de máquina em conjunto com a conteinerização do **Docker** foi utilizado nesse projeto.

## 🏫 Mais informações

O projeto mutant-developer-challenge foi desenvolvido para concluir um desafio solicitado pela empresa Mutant com intuito de participar de um processo seletivo.

Os seguintes dados são representados no programa:

1. Lista de websites de todos os usuários. ✅
2. Lista com nome, email e a empresa em que trabalha (em ordem alfabética). ✅
3. Mostrar todos os usuários que no endereço contem a palavra ```suite```. ✅
4. Salvar logs de todas interações no elasticsearch. ✅
5. EXTRA: Criar test unitário para validar os itens a cima. ✅


## 🔄 Pré-Requisitos

- [Vagrant - Criação e manipulação do ambiente virtual](https://www.vagrantup.com/)
- [VirtualBox - Virtualização](https://www.virtualbox.org/)

## 🤔 Como utilizar o projeto?

   Faça um clone deste repositório.

#### *** Observações ***

   - Tive problemas para instalar as dependências do node por dentro da VM. Será preciso rodar o comando "**yarn**" na máquina local dentro da pasta **/mutant-developer-challenge** para gerar a **node_modules**

   Na raíz do projeto, abra um terminal e digite o seguinte comando:


    vagrant up


   O Vagrant irá baixar a imagem da virtualização de modo on-line. Caso queira uma forma mais rápida de iniciar a VM, você pode fazer o download por aqui: **shorturl.at/uMNOS**

   Extraia o arquivo .zip na pasta:


    C:\Users\(usuario)\.vagrant.d\boxes


 Após o final da virtualização, acesse a máquina com o comando:


    vagrant ssh


 Dentro da virtualização digite o comando abaixo para acesso administrador da VM Debian:


    sudo su


 E acesse a pasta sincronizada do projeto:


    cd ../../vagrant/


 Vamos utilizar o Makefile para preparar nossas imagens do docker dentro da VM:


    make kibana


  aguarde...


    make elastic


  aguarde...


    make apm


  aguarde...


 O **elasticsearch** será responsável por armazenar os logs da nossa aplicação e vamos utilizar o **kibana** para a visualização desses logs. O **apm-server** será responsável pela conexão do elastic com a nossa aplicação NodeJS.

Agora com nossa imagens prontas, vamos instalar as ultimas dependências na VM para iniciarmos a aplicação:


    ./dep.sh


Esse shell será responsável por instalar o NodeJS, Yarn e Docker-compose em nossa VM.

## 🙋‍♂ Vamos rodar a aplicação!

 Nosso makefile vai iniciar a orquestração dos nossos containers em Docker:


    make start


 Aguarde a inicialização dos serviços e após isso, poderá acessar **http://localhost:8080/**

![Tela de índices das tarefas](/github/01.home.JPG)

## 🚦 Rotas de Interação

http:localhost:8080/websites

![Rota de Websites](/github/02.websites.JPG)

http:localhost:8080/ordem

![Rota de ordenação](/github/03.ordem.JPG)

http:localhost:8080/suite

![Rota de palavra suite](/github/04.suite.JPG)

http://localhost:5601/app/apm#/services/mutant-app/

![Acesso ao kibana](/github/05.kibana.JPG)

## 🧪 Utilizando o Jest para testes!

 Para rodar os testes em jest, basta rodar o comando em Makefile:


    make tests


## ⚠️ **Atenção**

### Caso ocorra algum erro durante o processo de criação ou manipulação da máquina virtual, você pode utilizar os comandos abaixo em seu terminal para retomar os processos: 😉

 Reiniciar a máquina virtual:


    vagrant reload


 Apagar a máquina virtual:


    vagrant destroy



# 📈 Obrigado!

Desenvolvido com ❤ Felipe da Silva Fernandes
