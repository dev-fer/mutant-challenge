n ?= 1
kibana:
				$(info Make: Pulling kibana tagged image.)
				n=$(n); \
				while [ $${n} -gt 0 ] ; do \
					docker pull "docker.elastic.co/kibana/kibana:7.6.2" && n=`expr $$n - 1` || n=`expr $$n = 1`; \
				done; \
				true

m ?= 1
elastic:
				$(info Make: Pulling elasticsearch tagged image.)
				m=$(m); \
				while [ $${m} -gt 0 ] ; do \
					docker pull "docker.elastic.co/elasticsearch/elasticsearch:7.6.2" && m=`expr $$m - 1` || m=`expr $$m = 1`; \
				done; \
				true

o ?= 1
apm:
				$(info Make: Pulling apm-server tagged image.)
				o=$(o); \
				while [ $${o} -gt 0 ] ; do \
					docker pull "docker.elastic.co/apm/apm-server:7.6.2" && o=`expr $$o - 1` || o=`expr $$o = 1`; \
				done; \
				true

start:
	$(info Make: Start project.)
	cd mutant-developer-challenge && docker-compose up

tests:
	$(info Make: Start tests projects.)
	cd mutant-developer-challenge && yarn test
